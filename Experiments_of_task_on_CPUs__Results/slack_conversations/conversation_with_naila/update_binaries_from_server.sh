#!/bin/sh

PROJECT_DIR="/home/lavoisier/svn_workspace/wapet/security/opportunistic_tasks_android"
SEPIA_PROJECT_DIR="/home/sepia"

cp $PROJECT_DIR/android/lineage/out/target/product/bramble/lineage-18.1-20210421-UNOFFICIAL-bramble.zip $PROJECT_DIR/android/lineage/out/target/product/bramble/lineage-18.1-20210421-UNOFFICIAL-bramble.zip_$(date +'%d%h%y_%H_%M')
cp $PROJECT_DIR/android/lineage/out/target/product/bramble/boot.img $PROJECT_DIR/android/lineage/out/target/product/bramble/boot.img_$(date +'%d%h%y_%H_%M')

scp -i $PROJECT_DIR/openssh_key_Tu_Dinh_Ngoc.key sepia@192.168.0.5:$SEPIA_PROJECT_DIR/android/lineage/out/target/product/bramble/boot.img  $PROJECT_DIR/android/lineage/out/target/product/bramble
scp -i $PROJECT_DIR/openssh_key_Tu_Dinh_Ngoc.key sepia@192.168.0.5:$SEPIA_PROJECT_DIR/android/lineage/out/target/product/bramble/lineage-18.1-20210421-UNOFFICIAL-bramble.zip  $PROJECT_DIR/android/lineage/out/target/product/bramble
