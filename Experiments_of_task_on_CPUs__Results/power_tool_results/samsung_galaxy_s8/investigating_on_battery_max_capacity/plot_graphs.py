import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
phone_energy = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""



number_of_cpus = ["mouse",    "idle_BM"   ,"1-0_BM","0-1_BM"]
workload = [ 0.1, 0.1, 1.26, 3.98   ] # 0.1 are not computed values
phone_energy = [ 1308, 92685.55, 92730.40, 92743.84 ]
phone_power = [ 40.16, 2617.51,  2622.86,  2618.65]

indices = [1,2,3,4,5,6,7,8, 9, 10, 11, 12, 13,14,15,16,17, 18, 19, 20, 21, 22]
batt_capacity_max = [700, 700, 700, 700, 700, 739, 772, 809, 835, 858, 877, 907, 919, 930, 941, 957,975, 988, 990, 990, 990, 990 ]
interface = [29, 29, 88, 97 ,100, 100, 100, 100, 100, 100, 100, 100, 100,100,100,100, 100, 100, 100, 100, 100, 100 ]
cc_info = [1275000, 1280000, 2525000, 2714000, 2810000, 2902500, 3028500, 3113000, 3182000, 3244000, 3301000, 3388000, 3421000, 3451500, 3484500, 3526500, 3553500, 3574500, 3593000, 3609500, 3629500, 3631500]






fig, ax = plt.subplots()

# Plot linear sequence, and set tick labels to the same color
line_1 = ax.plot(indices , interface , color='red', label = "UI_battery_level", linestyle="-")
ax.tick_params(axis='y', labelcolor='red')



ax2 = ax.twinx()

# Plot exponential sequence, set scale to logarithmic and change tick color
line_2 = ax2.plot(indices,  cc_info, color='green', label = "cc_info", linestyle="--")
ax2.tick_params(axis='y', labelcolor='green')


lines = line_1+line_2
labs = [l.get_label() for l in lines]
ax.legend(lines, labs, loc=0)



# Add title and axis names
plt.title('Evolution cc_info and of the the UI battery level')
plt.xlabel('Measurements (the first 2 were taken well before the last ones) ')

plt.savefig("Samsung_Gal_S8_investigating_cc_info_user_battery_level.png")

plt.clf()
plt.cla()
plt.close()


##################################

fig, ax = plt.subplots()

# Plot linear sequence, and set tick labels to the same color
line_3 = ax.plot(indices , batt_capacity_max, color='red', label = "batt_capacitiy_max", linestyle="-")
ax.tick_params(axis='y', labelcolor='red')


ax2 = ax.twinx()

# Plot exponential sequence, set scale to logarithmic and change tick color
line_4 = ax2.plot(indices,  cc_info, color='green', label = "cc_info", linestyle="--")
ax2.tick_params(axis='y', labelcolor='green')





lines = line_3+line_4
labs = [l.get_label() for l in lines]
ax.legend(lines, labs, loc=0)


# Add title and axis names
plt.title('Evolution of cc_info and batt_capacity_max')
plt.xlabel('Measurements (the first 2 were taken well before the last ones) ')



plt.savefig("Samsung_Gal_S8_investigating_cc_info_batt_capacity_max.png")

plt.clf()
plt.cla()
plt.close()



"""
fig = plt.figure()
plt.bar(number_of_cpus,phone_power, width=0.4)
 
# Add title and axis names
plt.title('Avg power used by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')

plt.savefig("Samsung_Gal_S8_power_absorbed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()


fig = plt.figure()
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('Energy consumed')

plt.savefig("Samsung_Gal_S8_energy_consumed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload (\times 10E11)')

plt.savefig("Samsung_Gal_S8_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy/ Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Battery cpu/Workload ( \times 10E-11 )')

plt.savefig("Samsung_Gal_S8_ratio_energy_by_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()

################################################
"""


""" to delete
fig = plt.figure()

plt.plot(indices, batt_capacity_max, label = "cc_info", linestyle="-")
plt.plot(indices,  cc_info , label = "batt_capacitiy_max", linestyle="--")
plt.plot(indices, interface , label = "interface", linestyle="-.")


# Add title and axis names
plt.title('Evolution of battery description file contents, the UI battery level')
plt.xlabel('Measurements (the first 2 were taken well before the last ones) ')


plt.legend()
plt.savefig("Samsung_Gal_S8_investigating_batt_capacity_max.png")

plt.clf()
plt.cla()
plt.close()
###############################
"""