import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
phone_energy = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""



number_of_cpus = ["mouse",    "idle_BM"   ,"1-0_BM",   "0-1_BM"]
workload =      [ 0.1,         0.1,        1.66,       7.41   ] # 0.1 are not computed values
phone_energy =  [ 1308,         5915 ,      69596.675, 122138.0 ]
phone_power =   [ 40.16,        180,       2008.785,   3341.75]


workload_new_usb_cable =      [ 0.1,         0.1,        1.55,       7.63   ] 
phone_energy_new_usb_cable = [ 1308, 11047.73,  29285.405,    78408.415 ]
phone_power_new_usb_cable = [ 40.16, 336.07,    879.245 ,      2253.82  ]






x = np.arange(len(number_of_cpus))  # the label locations
width = 0.35  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, phone_power, width, label='With old usb cable')
rects2 = ax.bar(x + width/2, phone_power_new_usb_cable, width, label='With new usb cable')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_xlabel('Tested configuration')
ax.set_ylabel('AVG Power')
ax.set_title('Avg power used by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
ax.set_xticks(x, number_of_cpus)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()



plt.savefig("Google_pixel_charge_stop_level_50__power_absorbed_according_to_cpu.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()




"""
fig = plt.figure()

width = 0.35  
plt.bar( number_of_cpus,phone_power, width, label='With old usb cable')
plt.bar(number_of_cpus,phone_power_new_usb_cable, width, label='With new usb cable')

# Add title and axis names
plt.title('Avg power used by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')

plt.savefig("Google_pixel_charge_limit__power_absorbed_according_to_cpu.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()
"""
#################################################################""""


fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, phone_energy, width, label='With old usb cable')
rects2 = ax.bar(x + width/2, phone_energy_new_usb_cable, width, label='With new usb cable')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_xlabel('Tested configuration')
ax.set_ylabel('Energy consumed')
ax.set_title('Energy consumed by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
ax.set_xticks(x, number_of_cpus)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()



plt.savefig("Google_pixel_charge_stop_level_50__energy_consumed_according_to_cpu.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()





"""
fig = plt.figure()
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('Energy consumed')

plt.savefig("Google_pixel_charge_limit_energy_consumed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()
"""
#################################################



fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, workload, width, label='With old usb cable')
rects2 = ax.bar(x + width/2, workload_new_usb_cable, width, label='With new usb cable')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_xlabel('Tested configuration')
ax.set_ylabel(r'Workload (\times 10E11)')
ax.set_title('Workload according to the thread configuration \n BM = Battery at middle level (50%)')
ax.set_xticks(x, number_of_cpus)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()



plt.savefig("Google_pixel_charge_stop_level_50__workload_according_to_cpu.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()

"""
fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload (\times 10E11)')

plt.savefig("Google_pixel_charge_limit_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()
"""
################################################

fig, ax = plt.subplots()
rects1 = ax.bar(x - width/2, np.divide(phone_energy,workload), width, label='With old usb cable')
rects2 = ax.bar(x + width/2, np.divide(phone_energy_new_usb_cable,workload_new_usb_cable), width, label='With new usb cable')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_xlabel('Tested configuration')
ax.set_ylabel(r'Workload (\times 10E11)')
ax.set_title('Energy/ Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
ax.set_xticks(x, number_of_cpus)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)

fig.tight_layout()



plt.savefig("Google_pixel_charge_stop_level_50__energy_by_workload_according_to_cpu.png")
plt.legend(loc='upper left')
plt.clf()
plt.cla()
plt.close()

"""
fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy/ Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Battery cpu/Workload ( \times 10E-11 )')

plt.savefig("Google_pixel_charge_limit_ratio_energy_by_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()
"""
################################################
