import matplotlib.pyplot as plt
import numpy as np
import math



           #-->12: (2 little, 1 big)             //pour voir si les littles mènent la danse


"""
number_of_cpus = ["1_p-Big_stp","1-Big_stp", "1-0","0-1_b","0-1_B"]
workload = [ 1.45, 1.45, 1.55, 7.03,  7.69 ]
phone_energy = [ 33.7, 34.4,  33.3, 19.4,  26.1  ]
"""



number_of_cpus = ["mouse",    "idle_BM"   ,"1-0_BM","0-1_BM"]
workload = [ 0.1, 0.1,  1.64,  7.41   ] # 0.1 are not computed values
phone_energy = [ 1308,44956.84,69473.67,84355.74]
phone_power = [ 40.16,1326.03, 2008.50,2405.07]





fig = plt.figure()
plt.bar(number_of_cpus,phone_power, width=0.4)
 
# Add title and axis names
plt.title('Avg power used by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('AVG Power')

plt.savefig("Google_pixel_charge_limit__power_absorbed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()


fig = plt.figure()
plt.bar(number_of_cpus,phone_energy, width=0.4)
 
# Add title and axis names
plt.title('Energy consumed by the phone according to the thread configuration \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel('Energy consumed')

plt.savefig("Google_pixel_charge_limit_energy_consumed_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()

#################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(workload,100000000000), width=0.4)

# Add title and axis names
plt.title('New computed Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Workload (\times 10E11)')

plt.savefig("Google_pixel_charge_limit_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()
################################################

fig = plt.figure()
plt.bar(number_of_cpus,np.divide(phone_energy,workload), width=0.4)
# Add title and axis names
plt.title('Energy/ Workload according to the number of CPUs  \n BM = Battery at middle level (50%)')
plt.xlabel('Number of CPUs')
plt.ylabel(r'Battery cpu/Workload ( \times 10E-11 )')

plt.savefig("Google_pixel_charge_limit_ratio_energy_by_workload_according_to_cpu.png")

plt.clf()
plt.cla()
plt.close()

################################################
